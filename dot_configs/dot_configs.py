#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from os import path
from pathlib import Path
from functools import reduce
import operator
import json
from collections import MutableMapping


class NoneWrapper:
    def __init__(self):
        self.value = None

    def __name__(self):
        return str(None)

    def __str__(self):
        return str(None)

    def __repr__(self):
        return str(None)


class IntWrapper(int):
    def __init__(self, value):
        super().__init__()


class FloatWrapper(float):
    def __init__(self, value):
        super().__init__()


class StrWrapper(str):
    def __init__(self, value):
        super().__init__()


class ListWrapper(list):
    def __init__(self, value):
        super().__init__(value)


class TupleWrapper(tuple):
    def __init__(self, value):
        super().__init__()


class BoolWrapper(int):
    def __init__(self, value):
        super().__init__()


class AttributeWrapper(MutableMapping):
    """TODO: Docstring for AttributeWrapper."""

    wrappers = {"int": IntWrapper,
                "IntWrapper": IntWrapper,
                "float": FloatWrapper,
                "FloatWrapper": FloatWrapper,
                "str": StrWrapper,
                "StrWrapper": StrWrapper,
                "list": ListWrapper,
                "ListWrapper": ListWrapper,
                "tuple": TupleWrapper,
                "TupleWrapper": TupleWrapper,
                "bool": IntWrapper,
                "BoolWrapper": IntWrapper,
                "NoneType": NoneWrapper
                }

    def __init__(self, key, *args, **kwargs):
        self.key = key
        self._store = {}

    def set(self, key, value=''):
        #  print(args)
        if isinstance(value, str) and len(value) == 0:
            # set node without value
            aw = AttributeWrapper(key)
            self._store[key] = aw
            setattr(self, aw.key, aw)
            setattr(aw, '_previous', self)
            setattr(aw, '_type', "node")
        else:
            # set node with value
            if isinstance(value, dict):
                # set node
                aw = AttributeWrapper(key)
                aw._store = value
                setattr(self, aw.key, aw)
                setattr(aw, '_previous', self)
                setattr(aw, '_type', "node")
            else:
                # set leaf
                if value is None:
                    #  self._store[key] = AttributeWrapper.wrappers['NoneType']()
                    setattr(self, key, None)
                else:
                    aw = AttributeWrapper.wrappers[value.__class__.__name__](
                        value)
                    self._store[key] = value
                    setattr(self, key, aw)
                    setattr(aw, '_previous', self)
                    setattr(aw, '_type', "leaf")

    def deep_set(self, value, _path=[]):
        try:
            for el in _path:
                getattr(self, el)
        except AttributeError:
            pass

    def deep_get(self, _path=[]):
        try:
            return reduce(operator.getitem, _path, self)
        except KeyError:
            raise KeyError("Property does not exist.")

    def __getitem__(self, key):
        return self._store[key]

    def __setitem__(self, key, value):
        self._store[key] = value

    def __delitem__(self, key):
        del self._store[key]

    def __iter__(self):
        return iter(self._store)

    def __len__(self):
        return len(self._store)

    def __repr__(self):
        return str(self._store)


class Configurations():
    """Object holding configuration variables."""

    def __init__(self, config_file):
        self.configuration = AttributeWrapper("configurations")
        self.json_validated = False

        if isinstance(config_file, str) or isinstance(config_file, Path):
            self.configuration._store = self._import_configs(
                path.abspath(config_file))
            self.configuration._previous = None
        elif isinstance(config_file, dict):
            self.configuration._store = config_file

        self._parse_json(self.configuration._store)

    @property
    def validated(self):
        return self.json_validated

    def get_configuration(self):
        """docstring for get_configuration"""
        return self.configuration

    def _import_configs(self, conf_file):
        """Import configurations from tf_conf.json."""
        try:
            with open(str(conf_file), 'r') as infile:
                conf = json.load(infile)
            self.json_validated = True
            return conf
        except ValueError:
            print(
                "Decoding the JSON config file has failed.\
                Please make sure the format is correct.")
            self.json_validated = False
        except FileNotFoundError:
            raise FileNotFoundError("Configuration file not found.")

    def _parse_json(self, sub_tree, prev_node=None, prev_key=None):
        """Parses json object recursively and returns path and value."""

        if isinstance(sub_tree, dict):
            for key, value in sub_tree.items():

                try:
                    curr_node = getattr(prev_node, prev_key)
                except TypeError:
                    curr_node = self.configuration

                curr_node.set(key, value)
                setattr(curr_node, "_type", "node")
                self._parse_json(value, curr_node, key)
        else:
            try:
                prev_node.set(prev_key, sub_tree)
                setattr(getattr(prev_node, prev_key), "_type", "leaf")
            except AttributeError as e:
                print(f"prev_node: {prev_node}")
                print(f"prev_key: {prev_key}")
                print(f"sub_tree: {sub_tree}")
                print(e)
