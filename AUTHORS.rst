=======
Credits
=======

Development Lead
----------------

* Magnus "Loxosceles" Henkel <loxosceles@gmx.de>

Contributors
------------

None yet. Why not be the first?
