============
Installation
============

It is recommended to use a virtual environment to install **dot-configs**. Once your
virtual environment is activated, install via pip::

    $ pip install dot-configs
