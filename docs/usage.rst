========
Usage
========


An example of a JSON configuration file could look like this [#f1]_ :

.. code-block:: json

  {
    "architecture": {
      "convNet": {
        "spectrograms": {
          "frame_sizes": [
            1024,
            2048,
            4096
          ],
          "hop_size": 441,
          "num_bands": 40
        },
        "training_data": {
          "test_size": 0.1,
          "val_size": 0.2,
          "random_state": 42
        },
        "paths": {
          "files": {
            "train_logs": "train_logs.csv",
            "state": "state.json",
            "model": "model.hdf5"
          },
          "dirs": {
            "store": "/path/to/data_store",
            "sandbox": "/path/to/sandbox",
            "tests": "/path/to/test_dir"
          }
        },
        "overwrite": false,
        "default_params": null
      }
    }
  }

Note that these configurations can be nested. You can use `strings`, `floats`, `integers`,
`lists`, `booleans` and `null` values. The JSON format does not support `None`
directly but, once imported, `null` will be converted to `None`. The same goes for
`false/true` which will be converted to their upper case equivalents `False` and
`True`.

To use **dot-configs** in your project::

   import dot_configs as dc
   config_filepath = '/path/to/json_configuration_file'
   cfg = dc.Configurations(config_filepath).get_configurations()

Now you can address any of the values in the following way::

   cfg.architecture.convNet.paths.files.train_logs    # "train_logs.csv"

.. rubric:: Footnotes

.. [#f1] You can find this example JSON under ``examples`` in the repository.
