import sys
from os import path

__author__ = 'Magnus "Loxosceles" Henkel'
__email__ = 'loxosceles@gmx.de'
__version__ = '0.1.0'

ROOT_DIR = path.dirname(path.dirname(path.abspath(__file__)))
sys.path.insert(0, ROOT_DIR)

