=============================
Dot Configs
=============================

.. image:: https://badge.fury.io/py/dot-configs.svg
    :target: https://badge.fury.io/py/dot-configs

Import and handle project configurations as dot-separated configs object

Full documentation can be found here:
https://dot-configs.readthedocs.io/en/latest/


Features
--------

* Easy parsing and handling of project options by using a modifiable configuration object
* Simple setup
* Splittable configs
* Backlinks: Every end-node can trace back its connection to the root node
* Dot-seperated object chain, convenient for addressing nodes and properties
* Configurations can quickly be inspected inside a command line interpreter, like
  Jupyter/iPython

