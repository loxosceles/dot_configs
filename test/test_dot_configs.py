#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for `dot_configs` module.
"""

import pytest
import json
from os import path
from dot_configs.dot_configs import (
    Configurations, AttributeWrapper, IntWrapper)


DATA_JSON_DIR = pytest.DATA_ROOT


@pytest.fixture
def attributewrapper():
    return AttributeWrapper('test')


@pytest.fixture
@pytest.mark.datafiles(DATA_JSON_DIR)
def cfg(datafiles):
    return path.join(str(datafiles), 'conf.json')


@pytest.fixture
@pytest.mark.datafiles(DATA_JSON_DIR)
def cfg_min(datafiles):
    return path.join(str(datafiles), 'minimal.json')


@pytest.fixture
@pytest.mark.datafiles(DATA_JSON_DIR)
def cfg_bad(datafiles):
    return path.join(str(datafiles), 'bad.json')


@pytest.fixture
@pytest.mark.datafiles(DATA_JSON_DIR)
def config_dict(cfg):
    with open(cfg, 'r') as infile:
        d = json.load(infile)
    return d


@pytest.fixture
@pytest.mark.datafiles(DATA_JSON_DIR)
def configs(cfg):
    c = Configurations(cfg)
    c = c.get_configuration()
    return c


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_validate_flag_is_true_when_parsing_correctly(cfg):
    c = Configurations(cfg)
    assert c.json_validated is True


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_validate_flag_is_false_when_parsing_incorrectly(cfg_bad):
    c = Configurations(cfg_bad)
    assert c.json_validated is False


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_set_new_node_without_value(configs):
    configs.set('args')
    assert isinstance(configs.args, AttributeWrapper)


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_if_previous_links_work(configs):
    assert configs.architecture._previous == configs
    assert configs.architecture.cnn._previous == configs.architecture


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_set_new_node_without_value_has_correct_type(configs):
    configs.set('args')
    assert configs.args._type == "node"


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_set_new_node_with_value(configs):
    configs.architecture.set('args', 'test')
    assert configs.architecture.args == 'test'


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_set_new_node_with_value_has_correct_type(configs):
    configs.architecture.set('args', 'test')
    assert configs.architecture.args._type == 'leaf'


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_set_new_node_with_value_has_correct_type_on_leaf(configs):
    configs.architecture.set('args1')
    configs.architecture.args1.set('args2')
    assert configs.architecture.args1.args2._type == 'node'
    configs.architecture.args1.set('args2', 'test')
    assert configs.architecture.args1._type == 'node'
    assert configs.architecture.args1.args2._type == 'leaf'


@pytest.mark.skip
@pytest.mark.datafiles(DATA_JSON_DIR)
def test_set_new_node_and_value_on_list(configs):
    configs.deep_set(['new_prop', 'args'], '')
    assert configs.new_prop.args == ''


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_deep_get_node_and_value(configs):
    epochs = configs.deep_get(
        ['architecture', 'cnn', 'training_params', 'epochs'])
    assert epochs == 4


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_deep_get_node_only_returns_dict_object(configs, config_dict):
    tp = configs.deep_get(['architecture', 'cnn', 'training_params'])
    assert tp == config_dict["architecture"]["cnn"]["training_params"]


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_deep_get_get_list_with_leaf(configs, config_dict):
    full_list = ['architecture', 'cnn', 'training_params', 'epochs']
    assert configs.deep_get(full_list) == 4


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_deep_get_with_leaf_on_chain(configs, config_dict):
    c = configs.architecture.cnn.training_params
    value = ['epochs']
    assert c.deep_get(value) == 4


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_deep_get_get_list_without_leaf(configs, config_dict):
    full_list = ['architecture', 'cnn', 'training_params']
    train_params_dict = configs.architecture.cnn.training_params
    assert configs.deep_get(full_list) == train_params_dict


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_deep_get_get_empty_list_returns_configs_object(configs, config_dict):
    assert configs.deep_get([]) == configs


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_deep_get_returns_none_if_no_arg_list_is_passed(configs, config_dict):
    assert configs.deep_get() is configs


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_deep_get_raises_error_if_prop_not_exists(configs, config_dict):
    with pytest.raises(KeyError) as e:
        assert configs.deep_get(['architecture', 'not_there', 'cnn'])
    assert str(e.value) == "'Property does not exist.'"


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_int_type_on_attributewrapper(configs):
    assert configs.architecture.cnn.training_params.epochs == 4


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_list_type_on_attributewrapper(configs):
    assert configs.spectrograms.frame_sizes == [1024, 2048, 4096]


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_str_type_on_attributewrapper(configs):
    assert configs.paths.dirs.store == '/path/to/store'


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_float_type_on_attributewrapper(configs):
    assert configs.training_data.test_size == 0.10


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_bool_type_on_attributewrapper(configs):
    assert configs.architecture.cnn.audio_params.filterbank == True
    assert type(
        configs.architecture.cnn.audio_params.filterbank) == IntWrapper


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_configurations_accepts_dict():
    c = Configurations({"key1": "val1", "key2": "val2"})
    c = c.get_configuration()
    assert c == {"key1": "val1", "key2": "val2"}


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_configurations_accepts_valid_path(cfg_min):
    c = Configurations(cfg_min)
    c = c.get_configuration()
    assert c == {"key": "value"}


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_configurations_rejects_invalid_path():
    with pytest.raises(FileNotFoundError) as e:
        c = Configurations(path.join(pytest.DATA_ROOT, "invalid.json"))
        c = c.get_configuration()
    assert str(e.value) == "Configuration file not found."


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_none_type_on_attributewrapper(configs):
    assert configs.paths.dirs.sandbox is None


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_IntWrapper_behaves_like_int(attributewrapper, configs):
    IntWrapper = attributewrapper.wrappers["int"]
    number = IntWrapper(1)
    assert (number + number) == 2


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_FloatWrapper_behaves_like_float(attributewrapper, configs):
    FloatWrapper = attributewrapper.wrappers["float"]
    number = FloatWrapper(1.2)
    assert (number + number) == 2.4


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_StrWrapper_behaves_like_str(attributewrapper, configs):
    StrWrapper = attributewrapper.wrappers["str"]
    string = StrWrapper('string')
    assert (string + string) == "stringstring"


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_leaf_on_attributewrapper_is_correct_type(attributewrapper, configs):
    epochs_class = configs.architecture.cnn.training_params.epochs.__class__
    assert isinstance(epochs_class, attributewrapper.wrappers["int"].__class__)


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_attributewrapper_stores_internal_dict_correctly(configs, config_dict):
    assert configs.paths._store == config_dict["paths"]


@pytest.mark.datafiles(DATA_JSON_DIR)
def test_root_attributewrapper_stores_internal_dict_correctly(configs, config_dict):
    assert configs._store == config_dict
