#!/usr/bin/env python3

# Imports
import pytest
from os import path
from dot_configs import ROOT_DIR
from dot_configs.dot_configs import Configurations


def pytest_configure():
    pytest.DATA_ROOT = path.join(ROOT_DIR, 'test/data')

#  @pytest.fixture(scope="session")
#  def configs():
#      configs = Configurations(path.join(pytest.DATA_ROOT,
#                                         'conf.json')).get_configuration()
#      return configs
